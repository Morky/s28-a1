// Get all todos
// fetch('https://jsonplaceholder.typicode.com/todos')
// .then((response)=>response.json())
// .then((data)=>{
// 	let list = data.map((todo)=>{
// 		return todo.title;
// 	})
// 	console.log(list);
// })


// // Getting a specific to do list
// fetch('https://jsonplaceholder.typicode.com/todos/1')
// .then ((response) => response.json())
// .then ((data) => console.log(`The item "${data.title}" has a status of ${data.completed}`));


// Creating a to do list item using POST method

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New TITLE',
		completed: true,
		userId: 1
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data));


// Updating a to do list item using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'UPDATED TITLE',
		completed: false
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data))

// Updating a to do list item using PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: true,
		dateCompleted: '01/19/22'
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data));


// Deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=>response.json())
.then((data)=>console.log('Successfully Deleted!'))
